# No Distraction Center Theme

![No discraction center theme preview](preview.png)

This theme is a fork from the official SN [No Distraction Theme](https://github.com/standardnotes/no-distraction-theme) with some differences:

- Centered layout
- Display only the note title centered

## How to install

Open "Extensions" in Standard Notes and click on "Import Extension" button. Paste the following URL and press enter:

```
https://codeberg.org/karl/no-distraction-centered-theme/raw/branch/main/ext.json
```
